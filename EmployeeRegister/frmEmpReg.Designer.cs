﻿namespace EmployeeRegister
{
    partial class frmEmpReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpbxRegister = new System.Windows.Forms.GroupBox();
            this.gpbxImage = new System.Windows.Forms.GroupBox();
            this.btnUploadImage = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnNextRecord = new System.Windows.Forms.Button();
            this.btnPrevRecord = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.gpbxContacts = new System.Windows.Forms.GroupBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblEmergency = new System.Windows.Forms.Label();
            this.lblMob = new System.Windows.Forms.Label();
            this.lblLL = new System.Windows.Forms.Label();
            this.lblPerAdd = new System.Windows.Forms.Label();
            this.lblLocAdd = new System.Windows.Forms.Label();
            this.txtbxPerAdd = new System.Windows.Forms.TextBox();
            this.txtbxLocAdd = new System.Windows.Forms.TextBox();
            this.lblSignupStatus = new System.Windows.Forms.Label();
            this.txtbxBloodGrp = new System.Windows.Forms.TextBox();
            this.lblBloodGrp = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.txtbxEmailID = new System.Windows.Forms.TextBox();
            this.lblDesignation = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblEmailID = new System.Windows.Forms.Label();
            this.txtbxName = new System.Windows.Forms.TextBox();
            this.txtbxDOB = new System.Windows.Forms.TextBox();
            this.txtbxDesignation = new System.Windows.Forms.TextBox();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblEmpID = new System.Windows.Forms.Label();
            this.txtbxEmpID = new System.Windows.Forms.TextBox();
            this.gpbxLstEmp = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.openfdUploadImage = new System.Windows.Forms.OpenFileDialog();
            this.gpbxRegister.SuspendLayout();
            this.gpbxImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gpbxContacts.SuspendLayout();
            this.gpbxLstEmp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbxRegister
            // 
            this.gpbxRegister.Controls.Add(this.gpbxImage);
            this.gpbxRegister.Controls.Add(this.btnNextRecord);
            this.gpbxRegister.Controls.Add(this.btnPrevRecord);
            this.gpbxRegister.Controls.Add(this.btnDelete);
            this.gpbxRegister.Controls.Add(this.btnModify);
            this.gpbxRegister.Controls.Add(this.btnAdd);
            this.gpbxRegister.Controls.Add(this.gpbxContacts);
            this.gpbxRegister.Controls.Add(this.lblPerAdd);
            this.gpbxRegister.Controls.Add(this.lblLocAdd);
            this.gpbxRegister.Controls.Add(this.txtbxPerAdd);
            this.gpbxRegister.Controls.Add(this.txtbxLocAdd);
            this.gpbxRegister.Controls.Add(this.lblSignupStatus);
            this.gpbxRegister.Controls.Add(this.txtbxBloodGrp);
            this.gpbxRegister.Controls.Add(this.lblBloodGrp);
            this.gpbxRegister.Controls.Add(this.btnRegister);
            this.gpbxRegister.Controls.Add(this.txtbxEmailID);
            this.gpbxRegister.Controls.Add(this.lblDesignation);
            this.gpbxRegister.Controls.Add(this.lblName);
            this.gpbxRegister.Controls.Add(this.lblEmailID);
            this.gpbxRegister.Controls.Add(this.txtbxName);
            this.gpbxRegister.Controls.Add(this.txtbxDOB);
            this.gpbxRegister.Controls.Add(this.txtbxDesignation);
            this.gpbxRegister.Controls.Add(this.lblDOB);
            this.gpbxRegister.Controls.Add(this.lblEmpID);
            this.gpbxRegister.Controls.Add(this.txtbxEmpID);
            this.gpbxRegister.Dock = System.Windows.Forms.DockStyle.Top;
            this.gpbxRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbxRegister.Location = new System.Drawing.Point(0, 0);
            this.gpbxRegister.Margin = new System.Windows.Forms.Padding(4);
            this.gpbxRegister.Name = "gpbxRegister";
            this.gpbxRegister.Padding = new System.Windows.Forms.Padding(4);
            this.gpbxRegister.Size = new System.Drawing.Size(1179, 272);
            this.gpbxRegister.TabIndex = 18;
            this.gpbxRegister.TabStop = false;
            this.gpbxRegister.Text = "Registraton";
            // 
            // gpbxImage
            // 
            this.gpbxImage.Controls.Add(this.btnUploadImage);
            this.gpbxImage.Controls.Add(this.pictureBox1);
            this.gpbxImage.Location = new System.Drawing.Point(859, 23);
            this.gpbxImage.Name = "gpbxImage";
            this.gpbxImage.Size = new System.Drawing.Size(285, 126);
            this.gpbxImage.TabIndex = 31;
            this.gpbxImage.TabStop = false;
            this.gpbxImage.Text = "Image";
            // 
            // btnUploadImage
            // 
            this.btnUploadImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUploadImage.Location = new System.Drawing.Point(259, 100);
            this.btnUploadImage.Name = "btnUploadImage";
            this.btnUploadImage.Size = new System.Drawing.Size(17, 21);
            this.btnUploadImage.TabIndex = 1;
            this.btnUploadImage.Text = "+";
            this.btnUploadImage.UseVisualStyleBackColor = true;
            this.btnUploadImage.Click += new System.EventHandler(this.btnUploadImage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(158, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 109);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // btnNextRecord
            // 
            this.btnNextRecord.Location = new System.Drawing.Point(1050, 235);
            this.btnNextRecord.Name = "btnNextRecord";
            this.btnNextRecord.Size = new System.Drawing.Size(100, 30);
            this.btnNextRecord.TabIndex = 30;
            this.btnNextRecord.Text = ">>";
            this.btnNextRecord.UseVisualStyleBackColor = true;
            // 
            // btnPrevRecord
            // 
            this.btnPrevRecord.Location = new System.Drawing.Point(944, 235);
            this.btnPrevRecord.Name = "btnPrevRecord";
            this.btnPrevRecord.Size = new System.Drawing.Size(100, 30);
            this.btnPrevRecord.TabIndex = 29;
            this.btnPrevRecord.Text = "<<";
            this.btnPrevRecord.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(529, 235);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 30);
            this.btnDelete.TabIndex = 28;
            this.btnDelete.Text = "&Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(423, 235);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(100, 30);
            this.btnModify.TabIndex = 27;
            this.btnModify.Text = "&Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(320, 235);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(100, 30);
            this.btnAdd.TabIndex = 26;
            this.btnAdd.Text = "&Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // gpbxContacts
            // 
            this.gpbxContacts.Controls.Add(this.textBox3);
            this.gpbxContacts.Controls.Add(this.textBox2);
            this.gpbxContacts.Controls.Add(this.textBox1);
            this.gpbxContacts.Controls.Add(this.lblEmergency);
            this.gpbxContacts.Controls.Add(this.lblMob);
            this.gpbxContacts.Controls.Add(this.lblLL);
            this.gpbxContacts.Location = new System.Drawing.Point(440, 161);
            this.gpbxContacts.Name = "gpbxContacts";
            this.gpbxContacts.Size = new System.Drawing.Size(710, 57);
            this.gpbxContacts.TabIndex = 25;
            this.gpbxContacts.TabStop = false;
            this.gpbxContacts.Text = "Contacts";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(556, 23);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(148, 22);
            this.textBox3.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(311, 23);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(147, 22);
            this.textBox2.TabIndex = 4;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(83, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(161, 22);
            this.textBox1.TabIndex = 3;
            // 
            // lblEmergency
            // 
            this.lblEmergency.AutoSize = true;
            this.lblEmergency.Location = new System.Drawing.Point(464, 26);
            this.lblEmergency.Name = "lblEmergency";
            this.lblEmergency.Size = new System.Drawing.Size(86, 16);
            this.lblEmergency.TabIndex = 2;
            this.lblEmergency.Text = "Emergency";
            // 
            // lblMob
            // 
            this.lblMob.AutoSize = true;
            this.lblMob.Location = new System.Drawing.Point(250, 26);
            this.lblMob.Name = "lblMob";
            this.lblMob.Size = new System.Drawing.Size(55, 16);
            this.lblMob.TabIndex = 1;
            this.lblMob.Text = "Mobile";
            // 
            // lblLL
            // 
            this.lblLL.AutoSize = true;
            this.lblLL.Location = new System.Drawing.Point(6, 26);
            this.lblLL.Name = "lblLL";
            this.lblLL.Size = new System.Drawing.Size(71, 16);
            this.lblLL.TabIndex = 0;
            this.lblLL.Text = "LandLine";
            // 
            // lblPerAdd
            // 
            this.lblPerAdd.AutoSize = true;
            this.lblPerAdd.Location = new System.Drawing.Point(437, 95);
            this.lblPerAdd.Name = "lblPerAdd";
            this.lblPerAdd.Size = new System.Drawing.Size(144, 16);
            this.lblPerAdd.TabIndex = 24;
            this.lblPerAdd.Text = "Permanent Address";
            // 
            // lblLocAdd
            // 
            this.lblLocAdd.AutoSize = true;
            this.lblLocAdd.Location = new System.Drawing.Point(437, 25);
            this.lblLocAdd.Name = "lblLocAdd";
            this.lblLocAdd.Size = new System.Drawing.Size(108, 16);
            this.lblLocAdd.TabIndex = 23;
            this.lblLocAdd.Text = "Local Address";
            // 
            // txtbxPerAdd
            // 
            this.txtbxPerAdd.Location = new System.Drawing.Point(596, 92);
            this.txtbxPerAdd.Multiline = true;
            this.txtbxPerAdd.Name = "txtbxPerAdd";
            this.txtbxPerAdd.Size = new System.Drawing.Size(255, 57);
            this.txtbxPerAdd.TabIndex = 22;
            // 
            // txtbxLocAdd
            // 
            this.txtbxLocAdd.Location = new System.Drawing.Point(596, 23);
            this.txtbxLocAdd.Multiline = true;
            this.txtbxLocAdd.Name = "txtbxLocAdd";
            this.txtbxLocAdd.Size = new System.Drawing.Size(255, 57);
            this.txtbxLocAdd.TabIndex = 21;
            // 
            // lblSignupStatus
            // 
            this.lblSignupStatus.AutoSize = true;
            this.lblSignupStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignupStatus.Location = new System.Drawing.Point(8, 252);
            this.lblSignupStatus.Name = "lblSignupStatus";
            this.lblSignupStatus.Size = new System.Drawing.Size(45, 16);
            this.lblSignupStatus.TabIndex = 20;
            this.lblSignupStatus.Text = "label1";
            // 
            // txtbxBloodGrp
            // 
            this.txtbxBloodGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxBloodGrp.Location = new System.Drawing.Point(127, 196);
            this.txtbxBloodGrp.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxBloodGrp.Name = "txtbxBloodGrp";
            this.txtbxBloodGrp.Size = new System.Drawing.Size(293, 22);
            this.txtbxBloodGrp.TabIndex = 19;
            // 
            // lblBloodGrp
            // 
            this.lblBloodGrp.AutoSize = true;
            this.lblBloodGrp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBloodGrp.Location = new System.Drawing.Point(8, 199);
            this.lblBloodGrp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBloodGrp.Name = "lblBloodGrp";
            this.lblBloodGrp.Size = new System.Drawing.Size(84, 16);
            this.lblBloodGrp.TabIndex = 18;
            this.lblBloodGrp.Text = "Blood Group";
            // 
            // btnRegister
            // 
            this.btnRegister.Location = new System.Drawing.Point(636, 235);
            this.btnRegister.Margin = new System.Windows.Forms.Padding(4);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(100, 30);
            this.btnRegister.TabIndex = 16;
            this.btnRegister.Text = "&Register";
            this.btnRegister.UseVisualStyleBackColor = true;
            // 
            // txtbxEmailID
            // 
            this.txtbxEmailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxEmailID.Location = new System.Drawing.Point(127, 161);
            this.txtbxEmailID.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxEmailID.Name = "txtbxEmailID";
            this.txtbxEmailID.Size = new System.Drawing.Size(293, 22);
            this.txtbxEmailID.TabIndex = 14;
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation.Location = new System.Drawing.Point(8, 62);
            this.lblDesignation.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(80, 16);
            this.lblDesignation.TabIndex = 7;
            this.lblDesignation.Text = "Designation";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(8, 27);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 16);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "Name";
            // 
            // lblEmailID
            // 
            this.lblEmailID.AutoSize = true;
            this.lblEmailID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmailID.Location = new System.Drawing.Point(8, 165);
            this.lblEmailID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmailID.Name = "lblEmailID";
            this.lblEmailID.Size = new System.Drawing.Size(58, 16);
            this.lblEmailID.TabIndex = 13;
            this.lblEmailID.Text = "Email ID";
            // 
            // txtbxName
            // 
            this.txtbxName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxName.Location = new System.Drawing.Point(127, 23);
            this.txtbxName.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxName.Name = "txtbxName";
            this.txtbxName.Size = new System.Drawing.Size(293, 22);
            this.txtbxName.TabIndex = 6;
            // 
            // txtbxDOB
            // 
            this.txtbxDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxDOB.Location = new System.Drawing.Point(127, 127);
            this.txtbxDOB.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxDOB.Name = "txtbxDOB";
            this.txtbxDOB.Size = new System.Drawing.Size(293, 22);
            this.txtbxDOB.TabIndex = 12;
            this.txtbxDOB.UseSystemPasswordChar = true;
            // 
            // txtbxDesignation
            // 
            this.txtbxDesignation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxDesignation.Location = new System.Drawing.Point(127, 58);
            this.txtbxDesignation.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxDesignation.Name = "txtbxDesignation";
            this.txtbxDesignation.Size = new System.Drawing.Size(293, 22);
            this.txtbxDesignation.TabIndex = 8;
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.Location = new System.Drawing.Point(8, 130);
            this.lblDOB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(37, 16);
            this.lblDOB.TabIndex = 11;
            this.lblDOB.Text = "DOB";
            // 
            // lblEmpID
            // 
            this.lblEmpID.AutoSize = true;
            this.lblEmpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpID.Location = new System.Drawing.Point(8, 96);
            this.lblEmpID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEmpID.Name = "lblEmpID";
            this.lblEmpID.Size = new System.Drawing.Size(86, 16);
            this.lblEmpID.TabIndex = 9;
            this.lblEmpID.Text = "Employee ID";
            // 
            // txtbxEmpID
            // 
            this.txtbxEmpID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbxEmpID.Location = new System.Drawing.Point(127, 92);
            this.txtbxEmpID.Margin = new System.Windows.Forms.Padding(4);
            this.txtbxEmpID.Name = "txtbxEmpID";
            this.txtbxEmpID.Size = new System.Drawing.Size(293, 22);
            this.txtbxEmpID.TabIndex = 10;
            this.txtbxEmpID.UseSystemPasswordChar = true;
            // 
            // gpbxLstEmp
            // 
            this.gpbxLstEmp.Controls.Add(this.dataGridView1);
            this.gpbxLstEmp.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gpbxLstEmp.Location = new System.Drawing.Point(0, 279);
            this.gpbxLstEmp.Name = "gpbxLstEmp";
            this.gpbxLstEmp.Size = new System.Drawing.Size(1179, 222);
            this.gpbxLstEmp.TabIndex = 19;
            this.gpbxLstEmp.TabStop = false;
            this.gpbxLstEmp.Text = "Registered Employees";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 18);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1173, 201);
            this.dataGridView1.TabIndex = 0;
            // 
            // openfdUploadImage
            // 
            this.openfdUploadImage.FileName = "openfdUploadImage";
            // 
            // frmEmpReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1179, 501);
            this.Controls.Add(this.gpbxLstEmp);
            this.Controls.Add(this.gpbxRegister);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "frmEmpReg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Register";
            this.gpbxRegister.ResumeLayout(false);
            this.gpbxRegister.PerformLayout();
            this.gpbxImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gpbxContacts.ResumeLayout(false);
            this.gpbxContacts.PerformLayout();
            this.gpbxLstEmp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbxRegister;
        private System.Windows.Forms.Label lblPerAdd;
        private System.Windows.Forms.Label lblLocAdd;
        private System.Windows.Forms.TextBox txtbxPerAdd;
        private System.Windows.Forms.TextBox txtbxLocAdd;
        private System.Windows.Forms.Label lblSignupStatus;
        private System.Windows.Forms.TextBox txtbxBloodGrp;
        private System.Windows.Forms.Label lblBloodGrp;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.TextBox txtbxEmailID;
        private System.Windows.Forms.Label lblDesignation;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblEmailID;
        private System.Windows.Forms.TextBox txtbxName;
        private System.Windows.Forms.TextBox txtbxDOB;
        private System.Windows.Forms.TextBox txtbxDesignation;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblEmpID;
        private System.Windows.Forms.TextBox txtbxEmpID;
        private System.Windows.Forms.GroupBox gpbxContacts;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblEmergency;
        private System.Windows.Forms.Label lblMob;
        private System.Windows.Forms.Label lblLL;
        private System.Windows.Forms.GroupBox gpbxLstEmp;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnNextRecord;
        private System.Windows.Forms.Button btnPrevRecord;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox gpbxImage;
        private System.Windows.Forms.Button btnUploadImage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openfdUploadImage;
    }
}

