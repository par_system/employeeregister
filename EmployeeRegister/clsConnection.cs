﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace EmployeeRegister
{
    class clsConnection : IConnection
    {
        string strconnectionstring;
        string strselectstring;
        string straddstring;
        string strupdatestring;
        string strdeletestring;
        SqlDataAdapter da;
        SqlCommand cmd;
        SqlConnection con;
        DataTable dt;

        public string strConnectionString
        {
            get
            {
                //throw new NotImplementedException();
                return strconnectionstring;
            }
            set
            {
                strconnectionstring = value;
                //throw new NotImplementedException();
            }
        }

        public string strSelectString
        {
            get
            {
                //throw new NotImplementedException();
                return strselectstring;
            }
            set
            {
                //throw new NotImplementedException();
                strselectstring = value;
            }
        }

        public string strAddString
        {
            get
            {
                //throw new NotImplementedException();
                return straddstring;
            }
            set
            {
                //throw new NotImplementedException();
                straddstring =value;
            }
        }

        public string strUpdateString
        {
            get
            {
                //throw new NotImplementedException();
                return strupdatestring;
            }
            set
            {
                //throw new NotImplementedException();
                strupdatestring = value;
            }
        }

        public string strDeleteString
        {
            get
            {
                //throw new NotImplementedException();
                return strdeletestring;
            }
            set
            {
                //throw new NotImplementedException();
                strdeletestring = value;
            }
        }

        public SqlDataAdapter DA
        {
            get
            {
                //throw new NotImplementedException();
                return da;
            }
            set
            {
                //throw new NotImplementedException();
                da = value;
            }
        }

        public SqlCommand CMD
        {
            get
            {
                //throw new NotImplementedException();
                return cmd;
            }
            set
            {
                //throw new NotImplementedException();
                cmd = value;
            }
        }

        public SqlConnection CON
        {
            get
            {
                //throw new NotImplementedException();
                return con;
            }
            set
            {
                //throw new NotImplementedException();
                con = value;
            }
        }

        public DataTable DT
        {
            get
            {
                //throw new NotImplementedException();
                return dt;
            }
            set
            {
                //throw new NotImplementedException();
                dt = value;
            }
        }
    }
}
