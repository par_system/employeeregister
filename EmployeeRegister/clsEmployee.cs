﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRegister
{
    class clsEmployee : IEmployee
    {
        private string strname;
        private string strdesignation;
        private string strempid;
        private DateTime dtdob;
        private string stremailid;
        private string strbloodgrp;
        private string strlocadd;
        private string strpermadd;
        private string strllnum;
        private string strmob;
        private string stremergency;

        public string strName
        {
            get
            {
                return strname;//throw new NotImplementedException();
            }
            set
            {
                strname = value;//throw new NotImplementedException();
            }
        }

        public string strDesignation
        {
            get
            {
                return strdesignation;// throw new NotImplementedException();
            }
            set
            {
                strdesignation = value;// throw new NotImplementedException();
            }
        }

        public string strEmpId
        {
            get
            {
                return strempid;//throw new NotImplementedException();
            }
            set
            {
                strempid = value;// throw new NotImplementedException();
            }
        }

        public DateTime dtDOB
        {
            get
            {
                return dtdob;// throw new NotImplementedException();
            }
            set
            {
                dtdob = value;// throw new NotImplementedException();
            }
        }

        public string strEmailId
        {
            get
            {
                return stremailid;// throw new NotImplementedException();
            }
            set
            {
                stremailid = value;// throw new NotImplementedException();
            }
        }

        public string strBloodGrp
        {
            get
            {
                return strbloodgrp;// throw new NotImplementedException();
            }
            set
            {
                strbloodgrp = value;// throw new NotImplementedException();
            }
        }

        public string strLocAdd
        {
            get
            {
                return strlocadd;// throw new NotImplementedException();
            }
            set
            {
                strlocadd = value;// throw new NotImplementedException();
            }
        }

        public string strPermAdd
        {
            get
            {
                return strpermadd;// throw new NotImplementedException();
            }
            set
            {
                strpermadd = value;// throw new NotImplementedException();
            }
        }

        public string strLLNum
        {
            get
            {
                return strllnum;// throw new NotImplementedException();
            }
            set
            {
                strllnum = value;// throw new NotImplementedException();
            }
        }

        public string strMob
        {
            get
            {
                return strmob;// throw new NotImplementedException();
            }
            set
            {
                strmob = value;// throw new NotImplementedException();
            }
        }

        public string strEmergency
        {
            get
            {
                return stremergency;// throw new NotImplementedException();
            }
            set
            {
                stremergency = value;// throw new NotImplementedException();
            }
        }

        public bool RegisterEmp(string m_Regname, string m_Regdesig, string m_Regempid, DateTime m_Regdob, string m_Regemailid, string m_Regbloodgrp, string m_Reglocadd, string m_Regperadd, string m_Regllnum, string m_Regmob, string m_Regemregency)
        {
            throw new NotImplementedException();
        }

        public bool UpdateEmp(string m_Updname, string m_Upddesig, string m_Updempid, DateTime m_Upddob, string m_Updemailid, string m_Updbloodgrp, string m_Updlocadd, string m_Updperadd, string m_Updllnum, string m_Updmob, string m_Updemregency)
        {
            throw new NotImplementedException();
        }

        public bool DeleteEmp(string m_Delname, string m_Delempid)
        {
            throw new NotImplementedException();
        }

        public int SearchAndCompare(string m_SACname, string m_SACempid)
        {
            throw new NotImplementedException();
        }
    }
}
