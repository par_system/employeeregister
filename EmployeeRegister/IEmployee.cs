﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeRegister
{
    interface IEmployee
    {
        //property signatures
        string strName { get; set; }
        string strDesignation { get; set; }
        string strEmpId { get; set; }
        DateTime dtDOB { get; set; }
        string strEmailId { get; set; }
        string strBloodGrp { get; set; }
        string strLocAdd { get; set; }
        string strPermAdd { get; set; }
        string strLLNum { get; set; }
        string strMob { get; set; }
        string strEmergency { get; set; }

        bool RegisterEmp(string m_Regname, string m_Regdesig, string m_Regempid, DateTime m_Regdob, string m_Regemailid, string m_Regbloodgrp, string m_Reglocadd, string m_Regperadd, string m_Regllnum, string m_Regmob, string m_Regemregency);
        bool UpdateEmp(string m_Updname, string m_Upddesig, string m_Updempid, DateTime m_Upddob, string m_Updemailid, string m_Updbloodgrp, string m_Updlocadd, string m_Updperadd, string m_Updllnum, string m_Updmob, string m_Updemregency);
        bool DeleteEmp(string m_Delname, string m_Delempid);
        int SearchAndCompare(string m_SACname, string m_SACempid);
    }
}
