﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using System.Data;

namespace EmployeeRegister
{
    interface IConnection
    {
        string strConnectionString { get; set; }
        string strSelectString { get; set; }
        string strAddString { get; set; }
        string strUpdateString { get; set; }
        string strDeleteString { get; set; }
        SqlDataAdapter DA { get; set; }
        SqlCommand CMD { get; set; }
        SqlConnection CON { get; set; }
        DataTable DT { get; set; }

    }
}
