﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace EmployeeRegister
{
    public partial class frmEmpReg : Form
    {
        clsConnection objclsConnection = new clsConnection();
        clsEmployee objclsEmployee = new clsEmployee();

        int intDBRowCount;
        
        long m_lImageFileLength = 0;
        byte[] m_barrImg;

        public frmEmpReg()
        {
            objclsConnection.strConnectionString = "Data Source=PAR06-PC;Initial Catalog=Employee;Integrated Security=SSPI;User ID=admin;Password=admin;";
            objclsConnection.strSelectString = "select * from RegEmp";
            objclsConnection.strAddString = "insert into RegEmp(SrNo, Name, Designation, EmpId, DOB, EmailID, BloodGroup, LocAdd, PerAdd, LindLine, Mobile, Emergency, AcCrDate, AcMdfDate) values (@SrNo, @Name, @Designation, @EmpId, @DOB, @EmailID, @BloodGroup, @LocAdd, @PerAdd, @LindLane, @Mobile, @Emergency, @AcCrDate, @AcMdfDate)";
            //objclsConnection.strUpdateString = "update RegEmp set Password =" + objclsEmployee + "where Name =" + objclsEmployee;
            //objclsConnection.strDeleteString = "delete form RegEmp where Name =" + objclsEmployee;

            InitializeComponent();
        }

        private void btnUploadImage_Click(object sender, EventArgs e)
        {
            try
            {
                this.openfdUploadImage.ShowDialog(this);
                string strFn = this.openfdUploadImage.FileName;
                this.pictureBox1.Image = Image.FromFile(strFn);
                FileInfo fiImage = new FileInfo(strFn);
                this.m_lImageFileLength = fiImage.Length;
                FileStream fs = new FileStream(strFn, FileMode.Open,
                                  FileAccess.Read, FileShare.Read);
                m_barrImg = new byte[Convert.ToInt32(this.m_lImageFileLength)];
                int iBytesRead = fs.Read(m_barrImg, 0, Convert.ToInt32(this.m_lImageFileLength));
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


    }
}
